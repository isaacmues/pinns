using Flux, Printf, Random, Plots, ProgressMeter, Statistics, CUDA, ForwardDiff, LinearAlgebra, DataFrames, UnicodePlots
using Elliptic
using Elliptic.Jacobi

# Freeze seed
Random.seed!(42)

# Constants
const g   = 9.81   |> Float32 # [m/s²] gravity
const l   = 0.5    |> Float32 # [m] length of the pendulum
const th0 = 0.25pi |> Float32 # [rad] Initial angle
const tf  = 2.0    |> Float32 # [s] Duration
const dtn = 0.001  |> Float32 # Nondimensional step 

# Nondimensional domain time (training points)
tn = rand(Float32, 1, 30)

# Training points
loader = Flux.DataLoader(tn |> gpu, batchsize=10, shuffle=true)

# Analytical solution
function sol(t)

    w = sqrt(g / l)
    a = sin(0.5 * th0)
    b = a^2
    c = K(b)

    @. 2.0 * asin(a * sn(c - w * t, b))
end

# Second derivative
d2(f, x, dx) = (f(x .+ dx) - 2.0 * f(x) + f(x .- dx)) / norm(dx)^2

# Neural network
nn = Chain(
           Dense(1  => 10, tanh),
           Dense(10 => 10, tanh),
           Dense(10 => 1)
          ) |> gpu

# Nondimensional trial solution
thn(nn, tn) = tn.^2 .* nn(tn) .+ 1f0

# Scaled trial solution
th(nn, tn) = thn(nn, tn) * th0

# Nondimensional differential equation remainder
function ode(nn, tn) 

    k = l * th0 / g / tf^2

    k * d2(tn -> thn(nn, tn), tn, dtn) +  sin.(th(nn, tn))
end

# Mean squared error
mse(t) = mean(abs2.(t))

# Loss
loss(nn, tn) = mse(ode(nn, tn))

# Optimizer
opt_state = Flux.setup(Flux.Adam(0.01), nn)

# Training Loop
printstyled("::: Pendulum :::\n", color=:green, bold=true)
printstyled("Training\n", color=:green, bold=true)
@showprogress for epoch in 1:5000
    Flux.train!((nn,tn) -> loss(nn, tn), nn, loader, opt_state)
end

# Rescaled domain
printstyled("\nResults\n", color=:green, bold=true)
tn = range(0, 1, 30) |> transpose |> tn -> map(Float32, tn)
t  = tn * tf |> transpose

results = DataFrame(
                    "time"     => t |> vec,
                    "analytic" => t |> sol |> vec,
                    "neural"   => tn |> gpu |> tn -> th(nn, tn) |> cpu |> vec
                   )

results.error = results[:, "analytic"] - results[:, "neural"] |> x -> map(abs, x)

println(results)
printstyled("\nMean squared loss: ", color=:green, bold=true)
println(loss(nn, tn |> gpu))

plt = Plot(;
           xlim=(0.0, tf),
           ylim=(-th0, th0),
           title="Pendulum",
           xlabel="time [s]",
           ylabel="angle [rad]",
           height=20,
           width=60
          )

plt = lineplot!(plt, results[:, "time"], results[:, "analytic"],
         name="Analytic solution",
         color=:red
        )

plt = lineplot!(plt, results[:, "time"], results[:, "neural"],
         name="Neural network",
         color=:green
        )

plt |> println
