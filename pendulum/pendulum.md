# Pendulum

## Differential equation

The differential equation of a pendulum of length $`l`$ and under a gravitational
acceleration $`g`$ is

```math
\frac{d^2\theta}{dt^2} = -\frac{g}{l}\sin\theta.
```

## Analytical Solution

If it starts at rest from a position $`\theta_0`$ the analytical solution is given
by

```math
\theta(t) = 2\arcsin\left\{
\sin\left(\frac{\theta_0}{2}\right)
\mathrm{sn}\left[\mathrm{K}\left(\sin^2\left(\frac{\theta_0}{2}\right)\right)
- \sqrt{\frac{g}{l}t}; \sin^2\left(\frac{\theta_0}{2}\right)\right]
\right\}
```

## PINN

Using the method described by Lagaris, Likas, and Fotiadis to solve the differential
equation using a neural network, $`NN`$, we can arrived at the following expression:

```math
\theta_{NN}(t) = t^2 NN(t) + \theta_0,
```

which satisfies the initial contitions.

### Results

The physical parameters are the following

| Parameter | Value |
| :-------: | :---: |
| $`g`$     | $`9.8\ \mathrm{m/s^2}`$ |
| $`l`$     | $`0.5\ \mathrm{m}`$ |
| $`\theta_0`$ | $`\pi / 4`$ |

And the results yielded by the script are plotted in the next figure

![Plots for the pendulum](./pendulum_plot.png)
